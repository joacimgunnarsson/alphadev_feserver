/**
 * Module Dependencies
 */
const restify = require("restify");

/**
 * Config
 */
const config = require("./config");

/**
 * Initialize Server
 */
const server = restify.createServer({
  name: config.name,
  version: config.version
});

/**
 * Bundled Plugins (http://restify.com/#bundled-plugins)
 */
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.fullResponse());
/*
 * Check performance settings
 */

const corsMiddleware = require("restify-cors-middleware");

const cors = corsMiddleware({
  origins: ["*"]
});

server.pre(cors.preflight);
server.use(cors.actual);

/**
 * Start Server, Connect to DB & Require Route Files
 */
server.listen(config.port, () => {
  const opts = {
    promiseLibrary: global.Promise,
    auto_reconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    autoIndex: true,
    useNewUrlParser: true,
    useCreateIndex: true
  };

  require("./routes/app")(server, restify);

  console.log(`Server is listening on port ${config.port}`);
});
