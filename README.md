# alphadev

Site consists of three repositorys. One for frontend, one for backend and one for holding a http-server that delivers the frontend code for clients.

## Install

_Make sure that you install all repositorys in the same folder_

```bash
/alphadev_fe
/alphadev_be
/alphadevwebserver
```

For FE Server:
_run npm install_ to install dependencies
_npm run start_ to start project

## Deploy

Deploys are automatically triggerd on push to the master branch of _alphadev_be_ and _alphadev_feserver_. This is done with Bitbucket Pipeline. What basically happens is that after succcesfull build it pushes to the heroku master branch which reflects the hosting environment.

To deploy frontend:

1. Merge changes to master branch in alphadev_feserver
2. Push

## FE Server

FE Server is only used in production to act as a HTTP server for the frontend.

## Production

Frontend https://alphadevwebserver.herokuapp.com/
Backend https://alphadevbe.herokuapp.com
